import React from 'react';

import Board from "./Board";
import {Usage} from "./Pieces";

import './App.css';

export default function App() {
  return (
      <>
        <Board />
        <Usage />
      </>
  );
}
