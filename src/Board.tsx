import * as React from "react";
import Square from "./Square";

const BOARD_SIZE = 10;

interface OwnState {
    pieces: { [key: number]: React.ReactNode | null};
}

class Board extends React.Component<{}, OwnState> {
    public state: OwnState = {
        pieces: this.initializePieces()
    };

    public render() {
        const squares = [...Array(BOARD_SIZE).keys()];

        return (
            <div style={{position: 'relative'}}>
                {squares.map(col => (
                    <div key={`d${col}`} style={{float: 'left'}}>
                        {squares.map(row => (
                            <SquareBackground key={`sb${col},${row}`} x={col} y={row}>
                            {({ backgroundColor }) =>
                                <Square key={`s${col},${row}`} color={backgroundColor}>
                                    {this.state.pieces[row * BOARD_SIZE + col]}
                                </Square>}
                            </SquareBackground>
                        ))}
                    </div>
                ))}
            </div>
        );
    }

    private initializePieces() {
        // create initial board configuration
        // and place pieces in respective spots
        return {}; // Array(BOARD_SIZE * BOARD_SIZE)
    }
}

interface RenderProp<TChildrenProps, TElement = any> {
    (props: TChildrenProps): React.ReactElement<TElement>;
}

interface SquareBackgroundProps {
    x: number;
    y: number;
    children: RenderProp<{ backgroundColor: "red" | "black" }>;
}

const SquareBackground: React.FunctionComponent<SquareBackgroundProps> = props => {
    if ((props.x + props.y) % 2)
        return props.children({ backgroundColor: "black" });
    else
        return props.children({ backgroundColor: "red" });
};

export default Board;
